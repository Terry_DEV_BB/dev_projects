package main

// IMPORTS -=-





import "io/ioutil"
import "path/filepath"

import "gopkg.in/yaml.v2"

import . "TerryCommon"

// END OF Imports -=-

func doExitStuff() {

	M.Print("\n\n === EXITING! (thats all folks!)\n\n")

} //end of cmdLineINit

type Rule struct {
    Desc  string  `yaml:"routeDesc"`
    Addy   string  `yaml:"routeUrl"`
}

type FileFormat struct {
   EndPoints []Rule  `yaml:"NGINX_Endpoints_2_Validate"`
}
//  *************************** MAIN AREA ****************************

func main() {

	//1. Startup Stuff
	DoInitStuff(PROGNAME, version)

    filename, _ := filepath.Abs("./nerv.config")
    yFile, err := ioutil.ReadFile(filename)

    CheckError(err, "Cant read from the nert.config file")
    
    var fileObj FileFormat
    
    err = yaml.Unmarshal(yFile, &fileObj)
    CheckError(err, "INVALID nert.config format!")
    
    flen := len(fileObj.EndPoints)
    
    
    
    for i := 0; i < flen; i++ {
        
        //m_desc := fileObj.EndPoints[i].Desc
        m_url := fileObj.EndPoints[i].Addy


        //M.Print("Hey the DESC is: ", m_desc, "\n")
        //M.Print("Hey the URL is: ", m_url, "\n")        
        checkUrl(m_url)
        
    }


	doExitStuff()

} // end of main
