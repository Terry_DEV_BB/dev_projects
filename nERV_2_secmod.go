package main
// ------------- IMPORTS ----------------



import "net/http"
import "io/ioutil"
import "strings"
import . "TerryCommon"

// import "strings"

// ---------------ENDofIMPORTS----------


var PROGNAME = "nERV - Nginx Endpoint Routing and Validation"
var version = "1.25"


func checkUrl(destUrl string)(string) {
    
    resp, err := http.Get(destUrl)
    
    result := "FAILED TO RESPOND\n"
    
    Y.Print(" *** Checking Route: ")
    C.Print(destUrl, " ... ")
    
    
    if (err != nil) {
        R.Print(result)
        return result  
    }
    
    
    defer resp.Body.Close()
    body, err := ioutil.ReadAll(resp.Body)
    
    //CheckError(err, "Cannot connect to the url: " + destUrl)
    
    if (err != nil) {
        
        R.Print(result)
        return result
    
    }
     
    
    btext := string(body)
    
    found := strings.Contains(btext, "404 Not Found")
    
    if (found) {
        R.Print(result)
        return result  

    }

    
    result = "Endpoint OK!!\n"
    G.Print(result)
    
    return result
} //end of checkUrl





